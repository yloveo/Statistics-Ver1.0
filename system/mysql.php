<?php
class Mysql
{
    private static $dbHost = 'localhost';//数据库地址
    private static $dbUser = '';//数据库用户
    private static $dbPass = '';//数据库密码
    private static $dbName = '';//数据库名
    private static $dbPort = '3306';//端口

    public static function getAllRows($query)
    {
        $connect = mysqli_connect(self::$dbHost, self::$dbUser, self::$dbPass, self::$dbName, self::$dbPort);
        mysqli_set_charset($connect,'utf8');
        $result = mysqli_query($connect, $query);
        $allrows = mysqli_fetch_all($result, MYSQLI_ASSOC);
        mysqli_close($connect);
        return $allrows;
    }
    public static function query($query)
    {
        $connect = mysqli_connect(self::$dbHost, self::$dbUser, self::$dbPass, self::$dbName, self::$dbPort);
        mysqli_set_charset($connect,'utf8');
        $result = mysqli_query($connect, $query);
        mysqli_close($connect);
        return $result;
    }
}
?>