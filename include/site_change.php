<?php
$head=<<<HEAD
<!doctype html><html lang="zh-cn"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"><title>修改数据-{$siteName}</title></head><body><div class="container"><br /><div class="text-center"><h3>修改数据</h3></div><hr /><nav aria-label="breadcrumb"><ol class="breadcrumb"><li class="breadcrumb-item"><a href="/">个人中心</a></li><li class="breadcrumb-item"><a href="change.php">网站管理</a></li><li class="breadcrumb-item active" aria-current="page">修改数据</li></ol></nav>
HEAD;
$alert2=<<<ALERT
<div class="alert alert-info alert-dismissible fade show" role="alert"><strong>修改成功，正在跳转至个人中心。</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
ALERT;
$alert=<<<ALERT
<div class="alert alert-info alert-dismissible fade show" role="alert"><strong>请输入完整的信息。</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
ALERT;
$form=<<<FORM
<form method="post" action="?siteid={$siteid}">
<div class="mb-3"><input class="form-control" type="text" name="sitename" value="{$site[0]['site_name']}" placeholder="网站名称"></div>
<div class="mb-3"><select class="form-control" name="sitekind"><option value="社区论坛">社区论坛</option><option value="网址导航">网址导航</option><option value="图片文学">图片文学</option><option value="技术建站">技术建站</option><option value="博客其他">博客其他</option></select></div>
<div class="mb-3"><select class="form-control" name="siteicon"><option value="1">图标一</option><option value="2">图标二</option><option value="3">图标三</option><option value="4">图标四</option><option value="5">图标五</option></select></div>
<div class="mb-3"><input class="form-control" type="text" name="sitecustom" value="{$site[0]['site_icon_custom']}" placeholder="自定义图标URL,填写此项则上一项无效"></div>
<div class="mb-3"><input class="form-control" type="text" name="sitepass" value="" placeholder="查看密码,留空则不设置"></div>
<div class="mb-3"><button type="submit" class="btn btn-outline-info btn-block">修改</button></div>
<div class="mb-3"><button class="btn btn-block btn-info" onclick="javascript:history.back(-1);">返回</button></div>
</form>
<hr />
<div class="card border-info mb-3"><div class="text-center card-header"><h5>图标类型</h5></div><div class="card-body">图标一：<img src="https://gitee.com/yloveo/gallery/raw/master/statistics/statistics1.png" /><hr />图标二：<img src="https://gitee.com/yloveo/gallery/raw/master/statistics/statistics2.png" /><hr />图标三：<img src="https://gitee.com/yloveo/gallery/raw/master/statistics/statistics3.png" /><hr />图标四：<img src="https://gitee.com/yloveo/gallery/raw/master/statistics/statistics4.png" /><hr />图标五：<img src="https://gitee.com/yloveo/gallery/raw/master/statistics/statistics5.png" /></div></div>
FORM;
$form1=<<<FORM
<form method="post" action="">
<div class="mb-3"><input class="form-control" type="text" name="sitename" placeholder="网站名称"></div>
<div class="mb-3"><select class="form-control" name="sitekind"><option value="社区论坛">社区论坛</option><option value="网址导航">网址导航</option><option value="图片文学">图片文学</option><option value="技术建站">技术建站</option><option value="博客其他">博客其他</option></select></div>
<div class="mb-3"><select class="form-control" name="siteicon"><option value="1">图标一</option><option value="2">图标二</option><option value="3">图标三</option><option value="4">图标四</option><option value="5">图标五</option></select></div>
<div class="mb-3"><input class="form-control" type="text" name="sitecustom" placeholder="自定义图标URL,填写此项则上一项无效"></div>
<div class="mb-3"><input class="form-control" type="text" name="sitepass" placeholder="查看密码,留空则不设置"></div>
<div class="mb-3"><button type="submit" class="btn btn-outline-info btn-block">添加</button></div>
<div class="mb-3"><button class="btn btn-block btn-info" onclick="javascript:history.back(-1);">返回</button></div>
</form>
<hr />
<div class="card border-info mb-3"><div class="text-center card-header"><h5>图标类型</h5></div><div class="card-body"></div></div>
FORM;
$foot=<<<FOOT
<hr /><div class="text-center"><p>&copy; 2018 {$siteName}</p></div></div><script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script><script src="https://cdn.bootcss.com/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script><script src="https://cdn.bootcss.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script></body></html>
FOOT;
?>