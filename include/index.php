<?php
$head=<<<HEAD
<!doctype html><html lang="zh-cn"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"><title>个人中心-{$siteName}</title></head><body><div class="container"><br /><div class="text-center"><h3>个人中心</h3></div><hr />
HEAD;
$center=<<<CENTER
<div class="card border-info mb-3"><div class="card-header text-center text-white bg-info"><h5>个人信息</h5></div><div class="card-body"><div class="mb-3"><ul class="list-group"><li class="list-group-item d-flex justify-content-between align-items-center">UID：<b>{$user[0]['user_id']}</b></li><li class="list-group-item d-flex justify-content-between align-items-center">用户名：<b>{$user[0]['user_name']}</b></li><li class="list-group-item d-flex justify-content-between align-items-center">邮箱：<b>{$user[0]['user_email']}</b></li><li class="list-group-item d-flex justify-content-between align-items-center">注册时间：<b>{$user[0]['user_reg_time']}</b></li><li class="list-group-item d-flex justify-content-between align-items-center">登录时间：<b>{$user[0]['user_login_time']}</b></li></ul></div></div></div>
<div class="card text-center border-info mb-3"><div class="card-header text-white bg-info"><h5>网站管理</h5></div><div class="card-body"><div class="mb-3"><a href="site/add.php"><button class="btn btn-outline-info btn-block">添加网站</button></a></div><div class="mb-3"><a href="site/change.php"><button class="btn btn-outline-info btn-block">修改数据</button></a></div><div class="mb-3"><a href="site/delete.php"><button class="btn btn-outline-info btn-block">删除网站</button></a></div><div class="mb-3"><a href="site/view.php"><button class="btn btn-outline-danger btn-block">查看数据</button></a></div></div></div>
<div class="card text-center border-info mb-3"><div class="card-header text-white bg-info"><h5>账户管理</h5></div><div class="card-body"><div class="mb-3"><a href="usermanage.php?locate=change"><button class="btn btn-outline-info btn-block">修改密码</button></a></div><div class="mb-3"><a href="usermanage.php?locate=outlogin"><button class="btn btn-outline-danger btn-block">退出登录</button></a></div></div></div>

CENTER;
$alert=<<<ALERT
<div class="alert alert-info alert-dismissible fade show" role="alert"><strong>你输入的账号或密码不正确，请输入正确的账户信息。</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
ALERT;
$form=<<<FORM
<form method="post" action="?locate=login">
<div class="mb-3"><input class="form-control" type="text" name="username" placeholder="用户名"></div>
<div class="mb-3"><input class="form-control" type="password" name="password" placeholder="密码"></div>
<div class="mb-3"><button type="submit" class="btn btn-outline-info btn-block">登录</button></div>
<div class="mb-3"><a href="?locate=reg"><button type="button" class="btn btn-info btn-block">注册</button></a></div>
</form>
FORM;
$foot=<<<FOOT
<hr /><div class="text-center"><p>&copy; 2018 {$siteName}</p></div></div><script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script><script src="https://cdn.bootcss.com/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script><script src="https://cdn.bootcss.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script></body></html>
FOOT;
?>