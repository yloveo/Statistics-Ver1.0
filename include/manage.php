<?php
$outlogin_head=<<<HEAD
<!doctype html><html lang="zh-cn"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"><title>账户管理-{$siteName}</title></head><body><div class="container"><br /><div class="text-center"><h3>账户管理</h3></div><hr /><nav aria-label="breadcrumb"><ol class="breadcrumb"><li class="breadcrumb-item"><a href="/">个人中心</a></li><li class="breadcrumb-item"><a href="usermanage.php?locate=change">账户管理</a></li><li class="breadcrumb-item active" aria-current="page">退出登录</li></ol></nav>
HEAD;
$outlogin_form=<<<FORM
<hr /><div class="text-center mb-3 border-info"><h5>确定退出登录？</h5></div><hr />
<div class="mb-3"><a href="usermanage.php?locate=outlogin&outlogin=yes"><button type="submit" class="btn btn-outline-info btn-block">是</button></a></div>
<div class="mb-3"><a href="/"><button type="submit" class="btn btn-info btn-block">否</button></a></div>
FORM;
$outlogin_alert=<<<ALERT
<div class="alert alert-info alert-dismissible fade show" role="alert"><strong>退出成功，正在跳转至首页。</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
ALERT;
$change_head=<<<HEAD
<!doctype html><html lang="zh-cn"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"><title>账户管理-{$siteName}</title></head><body><div class="container"><br /><div class="text-center"><h3>账户管理</h3></div><hr /><nav aria-label="breadcrumb"><ol class="breadcrumb"><li class="breadcrumb-item"><a href="/">个人中心</a></li><li class="breadcrumb-item"><a href="usermanage.php?locate=change">账户管理</a></li><li class="breadcrumb-item active" aria-current="page">修改密码</li></ol></nav>
HEAD;
$change_alert2=<<<ALERT
<div class="alert alert-info alert-dismissible fade show" role="alert"><strong>请输入完整的信息。</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
ALERT;
$change_alert1=<<<ALERT
<div class="alert alert-info alert-dismissible fade show" role="alert"><strong>原密码错误或者两次输入的新密码不同。</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
ALERT;
$change_alert3=<<<ALERT
<div class="alert alert-info alert-dismissible fade show" role="alert"><strong>修改成功，正在跳转自个人中心。</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
ALERT;
$change_form=<<<FORM
<form method="post" action="usermanage.php?locate=change">
<div class="mb-3"><input class="form-control" type="text" name="oldpass" placeholder="原密码"></div>
<div class="mb-3"><input class="form-control" type="text" name="newpassa" placeholder="新密码"></div>
<div class="mb-3"><input class="form-control" type="text" name="newpassb" placeholder="再次输入新密码"></div>
<div class="mb-3"><button type="submit" class="btn btn-outline-info btn-block">修改密码</button></div>
</form>
FORM;
$foot=<<<FOOT
<hr /><div class="text-center"><p>&copy; 2018 {$siteName}</p></div></div><script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script><script src="https://cdn.bootcss.com/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script><script src="https://cdn.bootcss.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script></body></html>
FOOT;
?>