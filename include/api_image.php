<?php
$head=<<<HEAD
<!doctype html><html lang="zh-cn"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"><title>错误提示-{$siteName}</title></head><body><div class="container"><br /><div class="text-center"><h3>错误提示</h3></div><hr />
HEAD;
$alert=<<<ALERT
<div class="alert alert-info alert-dismissible fade show" role="alert"><strong>Service Unavailable!</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
ALERT;
$center=<<<FORM
<div class="card border-info mb-3"><div class="card-header text-center text-white bg-info"><h5>查询结果</h5></div><div class="card-body"><div class="mb-3"><ul class="list-group"><li class="list-group-item d-flex justify-content-between align-items-center">IP：<b>{$ip->data->ip}</b></li><li class="list-group-item d-flex justify-content-between align-items-center">地区：<b>{$ip->data->country}</b></li><li class="list-group-item d-flex justify-content-between align-items-center">城市：<b>{$ip->data->city}</b></li><li class="list-group-item d-flex justify-content-between align-items-center">运营商：<b>{$ip->data->isp}</b></li></ul></div></div></div>
<div class="mb-3"><button class="btn btn-block btn-outline-info" onclick="javascript:history.back(-1);">返回</button></div>
FORM;
$foot=<<<FOOT
<hr /><div class="text-center"><p>&copy; 2018 {$siteName}</p></div></div><script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script><script src="https://cdn.bootcss.com/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script><script src="https://cdn.bootcss.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script></body></html>
FOOT;
?>