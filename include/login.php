<?php
$head=<<<HEAD
<!doctype html><html lang="zh-cn"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"><title>登录-{$siteName}</title></head><body><div class="container"><br /><div class="text-center"><h3>登录</h3></div><hr />
HEAD;
$alert=<<<ALERT
<div class="alert alert-info alert-dismissible fade show" role="alert"><strong>你输入的账号或密码不正确，请输入正确的账户信息。</strong><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
ALERT;
$form=<<<FORM
<form method="post" action="?locate=login">
<div class="mb-3"><input class="form-control" type="text" name="username" placeholder="用户名"></div>
<div class="mb-3"><input class="form-control" type="password" name="password" placeholder="密码"></div>
<div class="mb-3"><button type="submit" class="btn btn-outline-info btn-block">登录</button></div>
<div class="mb-3"><a href="?locate=reg"><button type="button" class="btn btn-info btn-block">注册</button></a></div>
</form>
FORM;
$foot=<<<FOOT
<hr /><div class="text-center"><p>&copy; 2018 {$siteName}</p></div></div><script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script><script src="https://cdn.bootcss.com/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script><script src="https://cdn.bootcss.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script></body></html>
FOOT;
?>