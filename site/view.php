<?php
include '../system/config.php';
include '../system/mysql.php';
if(!empty($_COOKIE[md5($siteKey)]) && ($user=Mysql::getAllRows('select * from user where user_cookie=\''.$_COOKIE[md5($siteKey)].'\''))){
if(!empty($_GET['siteid']) && ($site=Mysql::getAllRows('select * from site where site_id=\''.$_GET['siteid'].'\'')) && $site[0]['user_id']==$user[0]['user_id']){
if(empty($_GET['locate'])){
$locate='';
}else{
$locate=$_GET['locate'];
}
$yearStamp=strtotime(date('Y').'-01-01 00:00:00');
$monthStamp=strtotime(date('Y-m').'-01 00:00:00');
$weeknum=date('w');
if($weeknum==0){$week=7;}else{$week=$weeknum;}
$week--;
$yesStamp=strtotime(date('Y-m-d').'00:00:00');
$weekStamp=$yesStamp-($week*86400);
$nowStamp=time();
include '../include/site_view.php';
echo $head;
switch($locate){
case 'today':
$ip=Mysql::getAllRows('select count(*) from ip where site_id=\''.$site[0]['site_id'].'\' and ip_timestamp>=\''.$yesStamp.'\' and ip_timestamp<=\''.$nowStamp.'\'');
$pv=Mysql::getAllRows('select count(*) from pv where site_id=\''.$site[0]['site_id'].'\' and pv_timestamp>=\''.$yesStamp.'\' and pv_timestamp<=\''.$nowStamp.'\'');
$info=<<<INFO
<div class="card border-info mb-3"><div class="card-header text-center text-white bg-info"><h5>今日流量统计</h5></div><div class="card-body"><div class="mb-3"><ul class="list-group"><li class="list-group-item d-flex justify-content-between align-items-center">今日IP：<b>{$ip[0]['count(*)']}</b></li><li class="list-group-item d-flex justify-content-between align-items-center">今日PV：<b>{$pv[0]['count(*)']}</b></li></ul><hr /><div class="mb-3"><a href="?siteid={$site[0]['site_id']}&locate=iptoday"><button class="btn btn-block btn-outline-info">今日详细IP信息</button></a></div><div class="mb-3"><a href="?siteid={$site[0]['site_id']}&locate=pvtoday"><button class="btn btn-block btn-outline-info">今日详细PV信息</button></a></div></div><h5 class="text-center"><a class="btn btn-outline-info" href="?siteid={$site[0]['site_id']}&locate=today">今日</a>.<a class="btn btn-outline-info" href="?siteid={$site[0]['site_id']}&locate=week">本周</a>.<a class="btn btn-outline-info" href="?siteid={$site[0]['site_id']}&locate=month">本月</a>.<a class="btn btn-outline-info" href="?siteid={$site[0]['site_id']}&locate=year">今年</a>.<a class="btn btn-outline-info" href="?siteid={$site[0]['site_id']}&locate=all">所有</a></h5></div></div>
INFO;
echo $info;
break;
case 'week':
$ip=Mysql::getAllRows('select count(*) from ip where site_id=\''.$site[0]['site_id'].'\' and ip_timestamp>=\''.$weekStamp.'\' and ip_timestamp<=\''.$nowStamp.'\'');
$pv=Mysql::getAllRows('select count(*) from pv where site_id=\''.$site[0]['site_id'].'\' and pv_timestamp>=\''.$weekStamp.'\' and pv_timestamp<=\''.$nowStamp.'\'');
$info=<<<INFO
<div class="card border-info mb-3"><div class="card-header text-center text-white bg-info"><h5>本周流量统计</h5></div><div class="card-body"><div class="mb-3"><ul class="list-group"><li class="list-group-item d-flex justify-content-between align-items-center">本周IP：<b>{$ip[0]['count(*)']}</b></li><li class="list-group-item d-flex justify-content-between align-items-center">本周PV：<b>{$pv[0]['count(*)']}</b></li></ul><hr /><div class="mb-3"><a href="?siteid={$site[0]['site_id']}&locate=ipweek"><button class="btn btn-block btn-outline-info">本周详细IP信息</button></a></div><div class="mb-3"><a href="?siteid={$site[0]['site_id']}&locate=pvweek"><button class="btn btn-block btn-outline-info">本周详细PV信息</button></a></div></div><h5 class="text-center"><a class="btn btn-outline-info" href="?siteid={$site[0]['site_id']}&locate=today">今日</a>.<a class="btn btn-outline-info" href="?siteid={$site[0]['site_id']}&locate=week">本周</a>.<a class="btn btn-outline-info" href="?siteid={$site[0]['site_id']}&locate=month">本月</a>.<a class="btn btn-outline-info" href="?siteid={$site[0]['site_id']}&locate=year">今年</a>.<a class="btn btn-outline-info" href="?siteid={$site[0]['site_id']}&locate=all">所有</a></h5></div></div>
INFO;
echo $info;
break;
case 'month':
$ip=Mysql::getAllRows('select count(*) from ip where site_id=\''.$site[0]['site_id'].'\' and ip_timestamp>=\''.$monthStamp.'\' and ip_timestamp<=\''.$nowStamp.'\'');
$pv=Mysql::getAllRows('select count(*) from pv where site_id=\''.$site[0]['site_id'].'\' and pv_timestamp>=\''.$monthStamp.'\' and pv_timestamp<=\''.$nowStamp.'\'');
$info=<<<INFO
<div class="card border-info mb-3"><div class="card-header text-center text-white bg-info"><h5>本月流量统计</h5></div><div class="card-body"><div class="mb-3"><ul class="list-group"><li class="list-group-item d-flex justify-content-between align-items-center">本月IP：<b>{$ip[0]['count(*)']}</b></li><li class="list-group-item d-flex justify-content-between align-items-center">本月PV：<b>{$pv[0]['count(*)']}</b></li></ul><hr /><div class="mb-3"><a href="?siteid={$site[0]['site_id']}&locate=ipmonth"><button class="btn btn-block btn-outline-info">本月详细IP信息</button></a></div><div class="mb-3"><a href="?siteid={$site[0]['site_id']}&locate=pvmonth"><button class="btn btn-block btn-outline-info">本月详细PV信息</button></a></div></div><h5 class="text-center"><a class="btn btn-outline-info" href="?siteid={$site[0]['site_id']}&locate=today">今日</a>.<a class="btn btn-outline-info" href="?siteid={$site[0]['site_id']}&locate=week">本周</a>.<a class="btn btn-outline-info" href="?siteid={$site[0]['site_id']}&locate=month">本月</a>.<a class="btn btn-outline-info" href="?siteid={$site[0]['site_id']}&locate=year">今年</a>.<a class="btn btn-outline-info" href="?siteid={$site[0]['site_id']}&locate=all">所有</a></h5></div></div>
INFO;
echo $info;
break;
case 'year':
$ip=Mysql::getAllRows('select count(*) from ip where site_id=\''.$site[0]['site_id'].'\' and ip_timestamp>=\''.$yearStamp.'\' and ip_timestamp<=\''.$nowStamp.'\'');
$pv=Mysql::getAllRows('select count(*) from pv where site_id=\''.$site[0]['site_id'].'\' and pv_timestamp>=\''.$yearStamp.'\' and pv_timestamp<=\''.$nowStamp.'\'');
$info=<<<INFO
<div class="card border-info mb-3"><div class="card-header text-center text-white bg-info"><h5>今年流量统计</h5></div><div class="card-body"><div class="mb-3"><ul class="list-group"><li class="list-group-item d-flex justify-content-between align-items-center">今年IP：<b>{$ip[0]['count(*)']}</b></li><li class="list-group-item d-flex justify-content-between align-items-center">今年PV：<b>{$pv[0]['count(*)']}</b></li></ul><hr /><div class="mb-3"><a href="?siteid={$site[0]['site_id']}&locate=ipyear"><button class="btn btn-block btn-outline-info">今年详细IP信息</button></a></div><div class="mb-3"><a href="?siteid={$site[0]['site_id']}&locate=pvyear"><button class="btn btn-block btn-outline-info">今年详细PV信息</button></a></div></div><h5 class="text-center"><a class="btn btn-outline-info" href="?siteid={$site[0]['site_id']}&locate=today">今日</a>.<a class="btn btn-outline-info" href="?siteid={$site[0]['site_id']}&locate=week">本周</a>.<a class="btn btn-outline-info" href="?siteid={$site[0]['site_id']}&locate=month">本月</a>.<a class="btn btn-outline-info" href="?siteid={$site[0]['site_id']}&locate=year">今年</a>.<a class="btn btn-outline-info" href="?siteid={$site[0]['site_id']}&locate=all">所有</a></h5></div></div>
INFO;
echo $info;
break;
case 'all':
$ip=Mysql::getAllRows('select count(*) from ip where site_id=\''.$site[0]['site_id'].'\' and ip_timestamp<=\''.$nowStamp.'\'');
$pv=Mysql::getAllRows('select count(*) from pv where site_id=\''.$site[0]['site_id'].'\' and pv_timestamp<=\''.$nowStamp.'\'');
$info=<<<INFO
<div class="card border-info mb-3"><div class="card-header text-center text-white bg-info"><h5>所有流量统计</h5></div><div class="card-body"><div class="mb-3"><ul class="list-group"><li class="list-group-item d-flex justify-content-between align-items-center">所有IP：<b>{$ip[0]['count(*)']}</b></li><li class="list-group-item d-flex justify-content-between align-items-center">所有PV：<b>{$pv[0]['count(*)']}</b></li></ul><hr /><div class="mb-3"><a href="?siteid={$site[0]['site_id']}&locate=ipall"><button class="btn btn-block btn-outline-info">所有详细IP信息</button></a></div><div class="mb-3"><a href="?siteid={$site[0]['site_id']}&locate=pvall"><button class="btn btn-block btn-outline-info">所有详细PV信息</button></a></div></div><h5 class="text-center"><a class="btn btn-outline-info" href="?siteid={$site[0]['site_id']}&locate=today">今日</a>.<a class="btn btn-outline-info" href="?siteid={$site[0]['site_id']}&locate=week">本周</a>.<a class="btn btn-outline-info" href="?siteid={$site[0]['site_id']}&locate=month">本月</a>.<a class="btn btn-outline-info" href="?siteid={$site[0]['site_id']}&locate=year">今年</a>.<a class="btn btn-outline-info" href="?siteid={$site[0]['site_id']}&locate=all">所有</a></h5></div></div>
INFO;
echo $info;
break;
case 'iptoday':
$ip=Mysql::getAllRows('select * from ip where site_id=\''.$site[0]['site_id'].'\' and ip_timestamp>=\''.$yesStamp.'\' and ip_timestamp<=\''.$nowStamp.'\'');
echo '<div class="card mb-3" style="overflow:scroll;"><table class="table"><thead class="thead-light"><tr><th scope="col">IP</th><th scope="col">Time</th><th scope="col">UA</th></tr></thead><tbody>';
foreach($ip as $value){
echo '<tr><td><a href="/api/ip.php?ip='.$value['ip_ip'].'">'.$value['ip_ip'].'</a></a></td><td>'.$value['ip_datetime'].'</td><td>'.$value['ip_ua'].'</td></tr>';
}
echo '</tbody></table></div>';
break;
case 'ipweek':
$ip=Mysql::getAllRows('select * from ip where site_id=\''.$site[0]['site_id'].'\' and ip_timestamp>=\''.$weekStamp.'\' and ip_timestamp<=\''.$nowStamp.'\'');
echo '<div class="card mb-3" style="overflow:scroll;"><table class="table"><thead class="thead-light"><tr><th scope="col">IP</th><th scope="col">Time</th><th scope="col">UA</th></tr></thead><tbody>';
foreach($ip as $value){
echo '<tr><td><a href="/api/ip.php?ip='.$value['ip_ip'].'">'.$value['ip_ip'].'</a></a></td><td>'.$value['ip_datetime'].'</td><td>'.$value['ip_ua'].'</td></tr>';
}
echo '</tbody></table></div>';
break;
case 'ipmonth':
$ip=Mysql::getAllRows('select * from ip where site_id=\''.$site[0]['site_id'].'\' and ip_timestamp>=\''.$monthStamp.'\' and ip_timestamp<=\''.$nowStamp.'\'');
echo '<div class="card mb-3" style="overflow:scroll;"><table class="table"><thead class="thead-light"><tr><th scope="col">IP</th><th scope="col">Time</th><th scope="col">UA</th></tr></thead><tbody>';
foreach($ip as $value){
echo '<tr><td><a href="/api/ip.php?ip='.$value['ip_ip'].'">'.$value['ip_ip'].'</a></a></td><td>'.$value['ip_datetime'].'</td><td>'.$value['ip_ua'].'</td></tr>';
}
echo '</tbody></table></div>';
break;
case 'ipyear':
$ip=Mysql::getAllRows('select * from ip where site_id=\''.$site[0]['site_id'].'\' and ip_timestamp>=\''.$yearStamp.'\' and ip_timestamp<=\''.$nowStamp.'\'');
echo '<div class="card mb-3" style="overflow:scroll;"><table class="table"><thead class="thead-light"><tr><th scope="col">IP</th><th scope="col">Time</th><th scope="col">UA</th></tr></thead><tbody>';
foreach($ip as $value){
echo '<tr><td><a href="/api/ip.php?ip='.$value['ip_ip'].'">'.$value['ip_ip'].'</a></a></td><td>'.$value['ip_datetime'].'</td><td>'.$value['ip_ua'].'</td></tr>';
}
echo '</tbody></table></div>';
break;
case 'ipall':
$ip=Mysql::getAllRows('select * from ip where site_id=\''.$site[0]['site_id'].'\' and ip_timestamp<=\''.$nowStamp.'\'');
echo '<div class="card mb-3" style="overflow:scroll;"><table class="table"><thead class="thead-light"><tr><th scope="col">IP</th><th scope="col">Time</th><th scope="col">UA</th></tr></thead><tbody>';
foreach($ip as $value){
echo '<tr><td><a href="/api/ip.php?ip='.$value['ip_ip'].'">'.$value['ip_ip'].'</a></a></td><td>'.$value['ip_datetime'].'</td><td>'.$value['ip_ua'].'</td></tr>';
}
echo '</tbody></table></div>';
break;
case 'pvtoday':
$pv=Mysql::getAllRows('select * from pv where site_id=\''.$site[0]['site_id'].'\' and pv_timestamp>=\''.$yesStamp.'\' and pv_timestamp<=\''.$nowStamp.'\'');
echo '<div class="card mb-3" style="overflow:scroll;"><table class="table"><thead class="thead-light"><tr><th scope="col">IP</th><th scope="col">Time</th><th scope="col">Browser</th><th scope="col">Client</th><th scope="col">Source</th></tr></thead><tbody>';
foreach($pv as $value){
echo '<tr><td><a href="/api/ip.php?ip='.$value['pv_ip'].'">'.$value['pv_ip'].'</a></a></td><td>'.$value['pv_datetime'].'</td><td>'.$value['pv_browser'].'</td><td>'.$value['pv_client'].'</td><td>'.$value['pv_source'].'</td></tr>';
}
echo '</tbody></table></div>';
break;
case 'pvweek':
$pv=Mysql::getAllRows('select * from pv where site_id=\''.$site[0]['site_id'].'\' and pv_timestamp>=\''.$weekStamp.'\' and pv_timestamp<=\''.$nowStamp.'\'');
echo '<div class="card mb-3" style="overflow:scroll;"><table class="table"><thead class="thead-light"><tr><th scope="col">IP</th><th scope="col">Time</th><th scope="col">Browser</th><th scope="col">Client</th><th scope="col">Source</th></tr></thead><tbody>';
foreach($pv as $value){
echo '<tr><td><a href="/api/ip.php?ip='.$value['pv_ip'].'">'.$value['pv_ip'].'</a></a></td><td>'.$value['pv_datetime'].'</td><td>'.$value['pv_browser'].'</td><td>'.$value['pv_client'].'</td><td>'.$value['pv_source'].'</td></tr>';
}
echo '</tbody></table></div>';
break;
case 'pvmonth':
$pv=Mysql::getAllRows('select * from pv where site_id=\''.$site[0]['site_id'].'\' and pv_timestamp>=\''.$monthStamp.'\' and pv_timestamp<=\''.$nowStamp.'\'');
echo '<div class="card mb-3" style="overflow:scroll;"><table class="table"><thead class="thead-light"><tr><th scope="col">IP</th><th scope="col">Time</th><th scope="col">Browser</th><th scope="col">Client</th><th scope="col">Source</th></tr></thead><tbody>';
foreach($pv as $value){
echo '<tr><td><a href="/api/ip.php?ip='.$value['pv_ip'].'">'.$value['pv_ip'].'</a></a></td><td>'.$value['pv_datetime'].'</td><td>'.$value['pv_browser'].'</td><td>'.$value['pv_client'].'</td><td>'.$value['pv_source'].'</td></tr>';
}
echo '</tbody></table></div>';
break;
case 'pvyear':
$pv=Mysql::getAllRows('select * from pv where site_id=\''.$site[0]['site_id'].'\' and pv_timestamp>=\''.$yearStamp.'\' and pv_timestamp<=\''.$nowStamp.'\'');
echo '<div class="card mb-3" style="overflow:scroll;"><table class="table"><thead class="thead-light"><tr><th scope="col">IP</th><th scope="col">Time</th><th scope="col">Browser</th><th scope="col">Client</th><th scope="col">Source</th></tr></thead><tbody>';
foreach($pv as $value){
echo '<tr><td><a href="/api/ip.php?ip='.$value['pv_ip'].'">'.$value['pv_ip'].'</a></a></td><td>'.$value['pv_datetime'].'</td><td>'.$value['pv_browser'].'</td><td>'.$value['pv_client'].'</td><td>'.$value['pv_source'].'</td></tr>';
}
echo '</tbody></table></div>';
break;
case 'pvall':
$pv=Mysql::getAllRows('select * from pv where site_id=\''.$site[0]['site_id'].'\' and pv_timestamp<=\''.$nowStamp.'\'');
echo '<div class="card mb-3" style="overflow:scroll;"><table class="table"><thead class="thead-light"><tr><th scope="col">IP</th><th scope="col">Time</th><th scope="col">Browser</th><th scope="col">Client</th><th scope="col">Source</th></tr></thead><tbody>';
foreach($pv as $value){
echo '<tr><td><a href="/api/ip.php?ip='.$value['pv_ip'].'">'.$value['pv_ip'].'</a></a></td><td>'.$value['pv_datetime'].'</td><td>'.$value['pv_browser'].'</td><td>'.$value['pv_client'].'</td><td>'.$value['pv_source'].'</td></tr>';
}
echo '</tbody></table></div>';
break;
default:
$url=getUrl();
$url1=str_replace('&locate=information','',$url);
$imgurl=str_replace('site/view','api/image',$url1);
$viewurl=str_replace('site/view','api/view',$url1);
echo '<div class="card mb-3 border-info">
<div class="card-header bg-info text-white text-center">'.$site[0]['site_name'].'</div>
<div class="card-body"><div class="mb-3"><ul class="list-group"><li class="list-group-item d-flex justify-content-between align-items-center">网站名字：<b>'.$site[0]['site_name'].'</b></li><li class="list-group-item d-flex justify-content-between align-items-center">网站ID：<b>'.$site[0]['site_id'].'</b></li><li class="list-group-item d-flex justify-content-between align-items-center">网站类型：<b>'.$site[0]['site_kind'].'</b></li></ul></div><div class="form-group"><label>HTML调用地址</label><textarea class="form-control" rows="3"><a href="'.$viewurl.'"><img src="'.$imgurl.'" /></a></textarea></div><div class="form-group"><label>免登录查看地址</label><textarea class="form-control" rows="3">'.$viewurl.'</textarea></div></div>
</div>';
}
echo '<div class="mb-3"><button class="btn btn-block btn-outline-info" onclick="javascript:history.back(-1);">返回</button></div>';
echo $foot;
}else{
$sitelist=Mysql::getAllRows('select * from site where user_id=\''.$user[0]['user_id'].'\'');
include '../include/site_view.php';
echo $head;
foreach($sitelist as $value){
$es=<<<ESS
<div class="card border-info mb-3"><div class="card-header text-center text-white bg-info"><h5>{$value['site_name']}</h5></div><div class="card-body"><div class="mb-3"><ul class="list-group"><li class="list-group-item d-flex justify-content-between align-items-center">SiteID：<b>{$value['site_id']}</b></li><li class="list-group-item d-flex justify-content-between align-items-center">网站类型：<b>{$value['site_kind']}</b></li></ul></div>
<div class="mb-3"><a href="?siteid={$value['site_id']}&locate=information"><button class="btn btn-block btn-outline-info">使用信息</button></a></div>
<div class="mb-3"><a href="?siteid={$value['site_id']}&locate=today"><button class="btn btn-block btn-outline-info">流量数据</button></a></div>
</div></div>
ESS;
echo $es;
}
echo '<div class="mb-3"><button class="btn btn-block btn-outline-info" onclick="javascript:history.back(-1);">返回</button></div>';
echo $foot;
}
}else{
header('location:/');
}
function getUrl()
{
    $a = 'http';
    if ($_SERVER["HTTPS"] == "on") {
        $a .= "s";
    }
    $a .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $a .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $a .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }
    return $a;
}